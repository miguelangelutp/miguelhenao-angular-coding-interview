import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {
  
  // TODO: Use Subjects instead 
  
  gameCompleted: boolean = false;
  correctAnswers: number = 0;
  incorrectAnswers: number = 0;

  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe();


  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
  ) { }
    
  // TODO: create a destroyed$ subject and trigger it from the ngOnDestroy 



  ngOnInit(): void {
    // TODO: Unsubscirbe from this when destroying the component 
    this.questions$.subscribe(questions => {
      if (questions.length > 0 && questions.filter(q => !!q.selectedId).length == questions.length) {
        this.gameCompleted = true
      }
      this.correctAnswers = questions.filter(q => q.answers.find(x => x.isCorrect == true)?._id == q.selectedId).length
      this.incorrectAnswers = questions.length - this.correctAnswers;
    })
  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);

  }

}
